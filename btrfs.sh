#!/bin/bash

if [[ ! $1  ]]; then
	echo No device specified
	exit 1
elif [[ ! -b $1 ]]; then
	echo Device is not found
	exit 1
fi

SYSTEM_PART_NAME=cryptsystem

echo "GPT partition scheme will be created on ${1}:
   1. 550Mb FAT32 system/efi 
   2. 8gb SWAP
   3. The rest of the drive system BTRFS
      / - root subvol
      /home - home subvol
      /var/log - log subvol
      /.snapshots - snapshots sabvol

"

read -p "Press enter to continue..."


echo Clearing disk...

umount -R /mnt
swapoff -a
sgdisk --zap-all $1

echo Done. Creating new partition table



sgdisk --clear \
	--new=1:0:+550MiB  --typecode=1:ef00 --change-name=1:EFI \
	--new=2:0:+8GiB    --typecode=2:8200 --change-name=2:swap \
	--new=3:0:0        --typecode=3:8300 --change-name=3:system \
	$1

echo Partition table created. Creating file systems

sleep 2
echo Creating EFI fs...
mkfs.fat -F32 -n EFI /dev/disk/by-partlabel/EFI

sleep 2

echo Done. Creating swap  fs
mkswap -L swap /dev/disk/by-partlabel/swap

sleep 1
swapon -L swap 
sleep 1

echo preparing encryption
cryptsetup luksFormat /dev/disk/by-partlabel/system
cryptsetup luksOpen /dev/disk/by-partlabel/system $SYSTEM_PART_NAME

echo Done. Creating btrfs system...
mkfs.btrfs --force --label system /dev/mapper/$SYSTEM_PART_NAME
echo Done. Mounting...

sleep 1

o=defaults,x-mount.mkdir
o_btrfs=$o,compress=lzo,ssd,noatime

mount -t btrfs /dev/mapper/$SYSTEM_PART_NAME /mnt

echo Creating subvolumes...

btrfs subvolume create /mnt/@

sleep 1
btrfs subvolume create /mnt/@home


sleep 1
btrfs subvolume create /mnt/@home-snapshots

sleep 1
btrfs subvolume create /mnt/@snapshots

sleep 1
btrfs subvolume create /mnt/@log


sleep 1
btrfs subvolume create /mnt/@log-snapshots

sleep 1

echo Done. Unmounting..

umount -R /mnt

echo Mounting subvolumes

sleep 1
mount -t btrfs -o subvol=@,$o_btrfs /dev/mapper/$SYSTEM_PART_NAME /mnt

sleep 1
mount -t btrfs -o subvol=@home,$o_btrfs /dev/mapper/$SYSTEM_PART_NAME /mnt/home

sleep 1
mount -t btrfs -o subvol=@snapshots,$o_btrfs /dev/mapper/$SYSTEM_PART_NAME /mnt/.snapshots


sleep 1
mount -t btrfs -o subvol=@home-snapshots,$o_btrfs /dev/mapper/$SYSTEM_PART_NAME /mnt/home/.snapshots

sleep 1
mount -t btrfs -o subvol=@log,$o_btrfs /dev/mapper/$SYSTEM_PART_NAME /mnt/var/log


sleep 1
mount -t btrfs -o subvol=@log-snapshots,$o_btrfs /dev/mapper/$SYSTEM_PART_NAME /mnt/var/log/.snapshots

if [[ ! -d /mnt/boot ]]; then
	mkdir /mnt/boot
fi

mount LABEL=EFI /mnt/boot

echo Done.
