#!/bin/bash

echo checking hooks...

input="/etc/mkinitcpio.conf"
while IFS= read -r line
do
	if [[ "$line" == "HOOKS"* ]]; then

		if [[ "$line" != *"encrypt"* ]]; then
			echo Adding btrfs hook...
			sed -i '/^HOOKS.*block/s/block/& encrypt/' /etc/mkinitcpio.conf
		fi	

		
		sed -i   's/ udev / /' /etc/mkinitcpio.conf 
		sed -i   's/ resume / /' /etc/mkinitcpio.conf 
		sed -i   's/ usr / /' /etc/mkinitcpio.conf 

		if [[ "$line" != *"systemd"* ]]; then
			echo Adding systemd hook...
			sed -i '/^HOOKS.*base/s/base/& systemd/' /etc/mkinitcpio.conf 
		fi	

		break
	fi
done < "$input"

mkinitcpio -p linux

SYSTEM_PART_NAME=cryptroot

# UUID is printed in form of UUID="<uuid>", so extracting just UUID
UUID=$(blkid /dev/disk/by-partlabel/system | sed -r 's/^(.*)( UUID=.{37})(.*)/\2/; s/UUID="//; s/ //' )



sed -i -r "s|(^GRUB_CMDLINE_LINUX_DEFAULT=.*)(\"\$)|\1 cryptdevice=UUID=${UUID}:${SYSTEM_PART_NAME} root=/dev/mapper/${SYSTEM_PART_NAME}\"|" /etc/default/grub

grub-install --efi-directory=/boot --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg








