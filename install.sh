#!/bin/bash

pacstrap /mnt base base-devel linux linux-firmware btrfs-progs vi vim sudo dhcpcd netctl snapper git man wget curl grub efibootmgr

genfstab -U /mnt > /mnt/etc/fstab

cp init-grub.sh /mnt/root
cp personalize.sh /mnt/root
cp -r postinstall /mnt/root

echo Running personalize script
sleep 1
arch-chroot /mnt /root/personalize.sh


echo Initializing grub
sleep 1
arch-chroot /mnt /root/init-grub.sh
