#!/bin/bash
#


function ask_question(){
    while true; do
        read -p "$1 Y/n   " yn
        case $yn in
            [Yy]* )
                echo 1
                        return
                ;;
                [Nn]* )
                echo 0
                        return
                ;;

        esac
    done
}


function create_bootsnap_service(){
    echo "#!/bin/bash
for conf in \$(ls /etc/snapper/configs/); do
    snapper -c \$conf create  -c timeline  -d \"System boot\";
done
" | sudo  tee /usr/sbin/bootsnap.sh
    sudo chmod +x /usr/sbin/bootsnap.sh

    echo "[Unit]
Description=Snap on boot service

[Service]
ExecStart=/usr/sbin/bootsnap.sh

[Install]
WantedBy=multi-user.target
" | sudo tee /etc/systemd/system/bootsnap.service
    sudo systemctl enable  bootsnap

}

function create_snapper_config(){
    # $1 - config name
    # $2 - subvol path

    if [[ -f /etc/snapper/configs/$1 ]]; then
        echo "Config $1 already exists!"
        exit 1
    fi

    echo Unmounting snapshots...
    sleep 1
    sudo umount $2/.snapshots >/dev/null 2>/dev/null
    sudo rm -rf $2/.snapshots >/dev/null 2>/dev/null

    echo Creating configuration for $1...

    sudo snapper -c $1 create-config $2
    sudo btrfs subvolume delete $2/.snapshots

    echo Updating configuration...
    sleep 1

    # Setting pre-post time to live to 0 to remove useless snapshots
    sudo sed -r -i 's/^(EMPTY_PRE_POST_MIN_AGE=)(.*)/\1"0"/' /etc/snapper/configs/$1

    # Setting pre-post time to live to 0 to remove useless snapshots
    sudo sed -r -i 's/^(TIMELINE_MIN_AGE=)(.*)/\1"0"/' /etc/snapper/configs/$1
}

function setup_snapper(){

    create_snapper_config root /
    create_snapper_config user /home
    create_snapper_config log  /var/log


    echo Creating cleanup cron job...
    sleep 1
    # Cron job for cleaning up snapshots EVERY 3 hours. Edit as needed.
    # To edit run sudo crontab -e
    (sudo crontab -l ; echo "* */3 * * * snapper cleanup timeline && snapper cleanup empty-pre-post && snapper cleanup number 2>> /var/log/cron-error.log") 2>&1 | grep -v "no crontab" | sort | uniq | sudo crontab -

    echo Creating regular snapshot cron job...
    sleep 1

    # Hourly snapshots cron for root
    (sudo crontab -l ; echo "* */3 * * * snapper -c root create -d 'Regular snapshot' -c timeline 2>> /var/log/cron-error.log" ) 2>&1 | grep -v "no crontab" | sort | uniq | sudo crontab -


    sudo touch /var/log/cron-error.log
    sudo chmod 644 /var/log/cron-error.log

    echo Creating bootsnap startup service...
    create_bootsnap_service
    echo All set. Rebooting...
}

if [[ $(ask_question "This will create root user and log config for snapper. Continue?") -gt 0 ]]; then
    setup_snapper
    echo Please reboot now!
fi
