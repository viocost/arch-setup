#!/bin/bash




function ask_question(){
    while true; do
        read -p "$1 Y/n   " yn
        case $yn in
            [Yy]* )
                echo 1
                        return
                ;;
                [Nn]* )
                echo 0
                        return
                ;;

        esac
    done
}


function create_undervolt_daemon(){
    echo "
[Unit]
Description=undervolt
After=suspend.target
After=hibernate.target
After=hybrid-sleep.target

[Service]
Type=oneshot
# If you have installed undervolt globally (via sudo pip install):
# ExecStart=/usr/local/bin/undervolt -v --core -110 --cache -110
# If you want to run from source:
ExecStart=/usr/bin/undervolt -v --core -110 --cache -110 --gpu -60 -t 85

[Install]
WantedBy=multi-user.target
WantedBy=suspend.target
WantedBy=hibernate.target
WantedBy=hybrid-sleep.target
" | sudo tee /etc/systemd/system/undervolt.service
    sudo systemctl enable undervolt
    sudo systemctl start undervolt
}

sudo pacman -Sy linux-headers --noconfirm

sudo pacman -Sy nvidia nvidia-utils  nvidia-settings --noconfirm
sudo pacman -Sy nvidia-dkms --noconfirm

#LOOKS LIKE IT WORKS WITHOUT IT!
# echo '
# Section "OutputClass"
#     Identifier "nvidia"
#     MatchDriver "nvidia-drm"
#     Driver "nvidia"
#     Option "AllowEmptyInitialConfiguration"
#     Option "PrimaryGPU" "yes"
#     ModulePath "/usr/lib/nvidia/xorg"
#     ModulePath "/usr/lib/xorg/modules"
# EndSection
#
# ' |  sudo tee  /etc/X11/xorg.conf.d/10-nvidia-drm-outputclass.conf

echo '
xrandr --setprovideroutputsource modesetting NVIDIA-0
xrandr --auto
' | tee  ~/.xinitrc

sudo pacman -R xorg-xbacklight --noconfirm

sudo pacman -Sy acpilight acpi --noconfirm

# Undervolt for dell
yay -Sy python-undervolt --noconfirm
create_undervolt_daemon
