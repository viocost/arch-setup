#!/usr/bin/env bash

# installing yay
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si --noconfirm
cd ../

sudo pacman -Sy i3-gaps i3lock xautolock --noconfirm
sudo pacman -Sy dmenu --noconfirm
sudo pacman -Sy rofi --noconfirm

# notification daemon
sudo pacman -S dunst --noconfirm

# Installing cronie for cron jobs
sudo pacman -S cronie --noconfirm

# zip
sudo pacman -S unzip zip --noconfirm

# notification dialog
sudo pacman -S dialog --noconfirm

# X11 server
sudo pacman -Sy xorg xorg-xinit --noconfirm

# Wallpaper
sudo pacman -Sy nitrogen --noconfirm
sudo pacman -Sy picom --noconfirm

# Polybar
yay -Sy polybar --noconfirm



sudo pacman -Sy htop lm_sensors --noconfirm
sensors-detect

sudo pacman -Sy pulseaudio --noconfirm

pulseaudio --start

# fonts browser
sudo pacman -Sy gucharmap --noconfirm
sudo pacman -Sy noto-fonts noto-fonts-emoji ttf-linux-libertine --noconfirm

sudo pacman -Sy tilix thunar ranger w3m feh vifm --noconfirm

sudo pacman -Sy tldr --noconfirm


sudo pacman -Sy neovim --noconfirm

sudo pacman -Sy alsa pulseaudio --noconfirm
#1. Install oh-my-zsh

sudo pacman -Sy arandr --noconfirm

# Simple file watcher used to monitor logs and notify via dunst
sudo pacman -Sy entr --noconfirm
#

# open ssh
sudo pacman -Sy openssh --noconfirm
sudo systemctl enable sshd
sudo systemctl start sshd

# Devour allows launching GUI apps from terminal
# and hiding terminal, so it doesn't take extra space
yay -Sy devour --noconfirm
#Config file is "~/.zshrc"
#Plugins reside in ~/.oh-my-zsh.plugins
#
#2. Editing command in vim
#   Ctrl+e
#   the setting must be in .zshrcustom file
sudo pacman -Sy zsh --noconfirm

git clone https://gitlab.com/viocost/arch-rice ~/rice
git clone https://gitlab.com/viocost/arch-fonts ~/arch-fonts

# Goodies for wifi
sudo pacman -Sy networkmanager nm-connection-editor network-manager-applet --noconfirm
sudo pacman -R dhcpcd --noconfirm
sudo systemctl enable NetworkManager


# This installs oh-my-zsh and configures zsh to be default shell
# This thing should run always last
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
